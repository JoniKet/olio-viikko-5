/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.viikko.pkg5.t5;


/**
 *
 * @author Jönnsson
 */
abstract class Character {
    String name;
    String weapon;
    public Character(){
        name = " ";
        weapon = "";
    }
    public void useWeapon(){
        System.out.println("aseella "+weapon);
    }

    public void fight(){
        System.out.print(name + " tappelee ");
    }

class Queen extends Character{
    public Queen(){
        name = "Queen";
    }
}
class Knight extends Character{
    public Knight(){
        name = "Knight";
    }
}
class Troll extends Character{
    public Troll(){
        name = "Troll";
    }
}
class King extends Character{
    public King(){
        name = "King";
    }
}




class WeaponBehavior extends Character{
    String weapon;
    public WeaponBehavior(){
        weapon = "";
    }
    public void useWeapon(){
        System.out.println("aseella "+weapon);
    }
    

}

class SwordBehavior extends Character{
    public SwordBehavior(){
        weapon = "Sword";
    }
}
class ClubBehavior extends Character{
    public ClubBehavior(){
        weapon= "Club";
    }
}
class AxeBehavior extends Character{
    public AxeBehavior(){
        weapon = "Axe";
    }
}
class KnifeBehavior extends Character{
    public KnifeBehavior(){
        weapon = "Knife";
    }
}
}



