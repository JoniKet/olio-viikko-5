/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.viikko.pkg5.t5;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

/**
 *
 * @author Jönnsson
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic herecatcatcatcat
        
        int valinta1 = 10;
        int valinta2 = 10;
        int valinta3 = 10;
        Scanner scan = new Scanner(System.in);
        ByteArrayOutputStream ykkönen = new ByteArrayOutputStream();
        ByteArrayOutputStream kakkonen = new ByteArrayOutputStream();
        PrintStream henkilö = new PrintStream(ykkönen);
        PrintStream ase = new PrintStream(kakkonen);
        PrintStream old = System.out;

        
        while(valinta1 != 0){
            päävalikko();
            valinta1 = scan.nextInt();
   
            
            if(valinta1 == 1){
                ykkönen = null;
                kakkonen = null;
                henkilö = null;
                ase = null;
                ykkönen = new ByteArrayOutputStream();
                kakkonen = new ByteArrayOutputStream();
                henkilö = new PrintStream(ykkönen);
                ase = new PrintStream(kakkonen);
                old = System.out;
                taisteluvalikko();
                valinta2 = scan.nextInt();
                Character character = new Character() {};
                Character.WeaponBehavior weaponbehavior = character.new WeaponBehavior();

                
                switch (valinta2) {
                    
                    case 1:
                        Character.King king = character.new King();
                        System.setOut(henkilö);
                        king.fight();
                        System.out.flush();
                        System.setOut(old);
                        break;
                    case 2:
                        Character.Knight knight = character.new Knight();
                        System.setOut(henkilö);
                        knight.fight();
                        System.out.flush();
                        System.setOut(old);
                        
                        break;
                    case 3:
                        Character.Queen queen = character.new Queen();
                        System.setOut(henkilö);
                        queen.fight();
                        System.out.flush();
                        System.setOut(old);
                        
                        break;
                    case 4:
                        Character.Troll troll = character.new Troll();
                        System.setOut(henkilö);
                        troll.fight();
                        System.out.flush();
                        System.setOut(old);
                        break;
                    default:
                        break;
                }
                asevalikko();
                valinta3 = scan.nextInt();
                switch (valinta3) {
                    case 1:   
                        Character.WeaponBehavior.KnifeBehavior knifebehavior = weaponbehavior.new KnifeBehavior();
                        System.setOut(ase);
                        knifebehavior.useWeapon();
                        System.out.flush();
                        System.setOut(old);
                        break;
                    case 2:
                        Character.WeaponBehavior.AxeBehavior axebehavior = weaponbehavior.new AxeBehavior();
                        System.setOut(ase);
                        axebehavior.useWeapon();
                        System.out.flush();
                        System.setOut(old);
                        break;
                    case 3:
                        Character.WeaponBehavior.SwordBehavior swordbehavior = weaponbehavior.new SwordBehavior();
                        System.setOut(ase);
                        swordbehavior.useWeapon();
                        System.out.flush();
                        System.setOut(old);
                        break;
                    case 4:
                        Character.WeaponBehavior.ClubBehavior clubbehavior = weaponbehavior.new ClubBehavior();
                        System.setOut(ase);
                        clubbehavior.useWeapon();
                        System.out.flush();
                        System.setOut(old);
                        break;
                    default:
                        break;
                }
  
                
            }
            else if(valinta1 == 2){
                System.out.print(ykkönen.toString()+ kakkonen.toString());
                
            }
            
        }
    
    }
        
    public static void päävalikko(){
        System.out.println("*** TAISTELUSIMULAATTORI ***");
        System.out.println("1) Luo hahmo");
        System.out.println("2) Taistele hahmolla");
        System.out.println("0) Lopeta");
        System.out.print("Valintasi: ");
    }
    
    public static void taisteluvalikko(){
        System.out.println("Valitse hahmosi: ");
        System.out.println("1) Kuningas");
        System.out.println("2) Ritari");
        System.out.println("3) Kuningatar");
        System.out.println("4) Peikko");
        System.out.print("Valintasi: ");
    }
    public static void asevalikko(){
        System.out.println("Valitse aseesi: ");
        System.out.println("1) Veitsi");
        System.out.println("2) Kirves");
        System.out.println("3) Miekka");
        System.out.println("4) Nuija");
        System.out.print("Valintasi: ");
    }
    
}
